import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../components/Login'
import Dashboard from '../components/Dashboard'
import Personnel from '../components/Personnel'
import Schedule from '../components/Schedule'
import Materials from '../components/Materials'
import Users from '../components/Users'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Login',
    component: Login
  },
  {
    path: '/Dashboard',
    name: 'Dashboard',
    component: Dashboard
  },
  {
    path: '/Personnels',
    name: 'Personnel',
    component: Personnel
  },
  {
    path: '/Schedules',
    name: 'Schedule',
    component: Schedule
  },
  {
    path: '/Materials',
    name: 'Materials',
    component: Materials
  },
  {
    path: '/Users',
    name: 'Users',
    component: Users
  },
  
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
