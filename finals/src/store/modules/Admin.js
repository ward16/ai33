import axios from 'axios'

export default {
    namespaced:true,
    state: () => ({
        admin:[]
    }),
    mutations:{
        FETCH_DATA(state,data){
            state.admin = data
        }
    },
    actions:{
        fetchData({commit}){
            axios.get('http://127.0.0.1:8000/api/admin')
            .then(res => {
                commit('FETCH_DATA',res.data)
            })
        }
    },
    getters:{
        fetch(state){
            return state.admin
        }
    }
}