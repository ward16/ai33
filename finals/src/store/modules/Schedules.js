import axios from 'axios'

export default {
    namespaced:true,
    state: () => ({
        schedules:[]
    }),
    mutations:{
        FETCH_DATA(state,data){
            state.schedules = data
        }
    },
    actions:{
        fetchData({commit}){
            axios.get('http://127.0.0.1:8000/api/schedules')
            .then(res => {
                commit('FETCH_DATA',res.data)
            })
        },

        addData({dispatch},data){
            axios.post('http://127.0.0.1:8000/api/schedules', data)
            .then( () => {
                dispatch('fetchData')
            })
        },

        updateData({dispatch},data){
            axios.put(`http://127.0.0.1:8000/api/schedules/${data.id}`, data)
            .then( () => {
                dispatch('fetchData')
            })
        },

        deleteData({dispatch},data){
            axios.delete(`http://127.0.0.1:8000/api/schedules/${data.id}`)
            .then( () => {
                dispatch('fetchData')
            })
        },
    },
    getters:{
        fetch(state){
            return state.schedules
        }
    }
}