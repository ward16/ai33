import axios from 'axios'

export default{

    namespaced:true,
    state: () => ({
        status:[],
    }),
    mutations:{
        FETCH_DATA(state,data){
            state.status = data
        }
    },
    actions:{
        fetchData({commit}){
            axios.get('http://127.0.0.1:8000/api/status')
            .then(res => {
                commit('FETCH_DATA',res.data)
            })
        }
    },
    getters:{
        fetch(state){
            return state.status
        }
    }
}