import Vue from 'vue'
import Vuex from 'vuex'
import personnels from '../store/modules/Personnels'
import materials from '../store/modules/Materials'
import schedules from '../store/modules/Schedules'
import status from '../store/modules/Status'
import admin from '../store/modules/Admin'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    personnels,
    materials,
    schedules,
    status,
    admin
  }
})
