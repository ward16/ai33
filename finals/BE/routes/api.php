<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;
use App\Http\Controllers\StatusController;
use App\Http\Controllers\MaterialsController;
use App\Http\Controllers\PersonnelsController;
use App\Http\Controllers\SchedulesController;
use App\Http\Controllers\DashboardController;
use App\Http\Controllers\UsersController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::resources([
    '/admin' => AdminController::class,
    '/status' => StatusController::class,
    '/materials' => MaterialsController::class,
    '/personnels' => PersonnelsController::class,
    '/schedules' => SchedulesController::class,
    '/dashboard' => DashboardController::class,
    '/users' => UsersController::class
]);

