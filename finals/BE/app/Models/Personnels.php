<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Personnels extends Model
{
    use HasFactory;

    protected $table = 'personnels';
    protected $fillable = ['first_name', 'last_name','middle_name','status_id'];

    public function status()
    {
        return $this->belongsTo(Status::class);
    }

    public function schedules()
    {
        return $this->hasMany(Schedules::class);
    }

}
