<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Schedules extends Model
{
    use HasFactory;

    protected $table = 'schedules';
    protected $fillable = ['personnels_id', 'time','destination','materials_id'];

    public function personnels()
    {
        return $this->belongsTo(Personnels::class);
    }

    public function materials()
    {
        return $this->belongsTo(Materials::class);
    }
}
