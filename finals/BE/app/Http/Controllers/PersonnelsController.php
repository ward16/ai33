<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Personnels;


class PersonnelsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $personnels = Personnels::with('status')->get();

        return response()->json($personnels);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $personnels = new Personnels;
        $personnels->first_name = $request->input('first_name');
        $personnels->last_name = $request->input('last_name');
        $personnels->middle_name = $request->input('middle_name');
        $personnels->status_id = $request->input('status_id');
        
        $personnels->save();

        return response()->json($personnels);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {   
        $personnels = Personnels::find($id);
        $personnels->first_name = $request->input('first_name');
        $personnels->last_name = $request->input('last_name');
        $personnels->middle_name = $request->input('middle_name');
        $personnels->status_id = $request->input('status_id');
        
        $personnels->save();

        return response()->json($personnels);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $personnels = Personnels::find($id);
        $personnels->delete();

        return response()->json($personnels);
    }
}
