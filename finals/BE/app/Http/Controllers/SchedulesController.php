<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\Schedules;
use App\Models\Materials;

class SchedulesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {   
        $schedules = Schedules::with('personnels','materials')->get();
        return response()->json($schedules);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $schedules = new Schedules;
        $schedules->personnels_id = $request->input('personnels_id');
        $schedules->time = $request->input('time');
        $schedules->destination = $request->input('destination');
        $schedules->materials_id = $request->input('materials_id');
        $schedules->save();

        return response()->json($schedules);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $schedules = Schedules::find($id);
        $schedules->personnels_id = $request->input('personnels_id');
        $schedules->time = $request->input('time');
        $schedules->destination = $request->input('destination');
        $schedules->materials_id = $request->input('materials_id');
        
        $schedules->save();

        return response()->json($schedules);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $schedules = Schedules::find($id);
        $schedules->delete();

        return response()->json($schedules);

    }
}
